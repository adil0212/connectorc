// <!-- 组件名: connector -->
// <!-- 创建日期: 2023/12/28 -->
// <!-- 编写者: Adil -->
import request from "@/utils/request";

export const GetConnectorAdd = (data) => {
    return request({
        url: 'adminapi/project/connector/add',
        method: 'post',
        data
    })
}

export const GetConnectorList = (params) => {
    return request({
        url: '/adminapi/project/connector/getList',
        method: 'get',
        params
    })
}

export const GetConnectorListId = (id) => {
    return request({
        url: `/adminapi/project/connector/List/${id}`,
        method: 'get'
    })
}

export const GetConnectorUpdate = (id, data) => {
    return request({
        url: `/adminapi/project/connector/update/${id}`,
        method: "put",
        data
    })
}


export const GetConnectorDelete = (id) => {
    return request({
        url: `/adminapi/project/connector/delete/${id}`,
        method: 'delete'
    })
}


export const GetConnectorPreview = (id) => {
    return request({
        url: `/adminapi/project/connector/preview/${id}`,
        method: 'get'
    })
}
