// <!-- 组件名: menu -->
// <!-- 创建日期: 2023/12/23 -->
// <!-- 编写者: Adil -->
import request from '@/utils/request'

// 获取路由
export const getRouters = () => {
  return request({
    url: `adminapi/user/getRouter`,
    method: 'get'
  })
}

export const getRoutersID = (id) => {
  return request({
    url: `adminapi/user/getRouter/${id}`,
    method: 'get'
  })
}
