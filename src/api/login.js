// <!-- 组件名: login -->
// <!-- 创建日期: 2023/12/23 -->
// <!-- 编写者: Adil -->
import request from "@/utils/request";

export function login(username, password) {
  const data = {
    username,
    password
  }
  return request({
    url: 'adminapi/user/login',
    headers: {
      isToken: false,
      repeatSubmit: false
    },
    method: 'post',
    data: data
  })
}

export const getInfo = (id) => {
  return request({
    url: `/adminapi/user/getInfo/${id}`,
    method: 'get'
  })
}

export const getInfos = () => {
  return request({
    url: '/adminapi/user/getInfos',
    method: 'get'
  })
}

