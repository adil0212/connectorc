// <!-- 组件名: project -->
// <!-- 创建日期: 2023/12/25 -->
// <!-- 编写者: Adil -->
import downloads from "@/utils/request";
import request from "@/utils/request";

export const GetProjectLimit = (params) => {
  return downloads({
    url: 'adminapi/project/getLimit',
    method: 'get',
    params
  })
}


export const GetProjectID = (id, author) => {
  return downloads({
    url: `/adminapi/project/list/${id}/${author}`,
    method: 'get'
  })
}


export const GetModuleAdd = (data) => {
  return request({
    url: 'adminapi/project/module/add',
    method: 'post',
    data
  })
}

export const GetModuleList = (params) => {
  return request({
    url: '/adminapi/project/module/getList',
    method: 'get',
    params
  })
}

export const GetDeletePro = (id) => {
  return request({
    url: `/adminapi/project/delete/${id}`,
    method: 'delete'
  })
}

export const GetUpdateModule = (id, params) => {
  return request({
    url: `/adminapi/project/update/${id}`,
    method: 'put',
    params
  })
}

export const GetDeleteModel = (id) => {
  return request({
    url: `/adminapi/project/module/delete/${id}`,
    method: 'delete'
  })
}

export const GetModelListId = (id) => {
  return request({
    url: `/adminapi/project/module/list/${id}`,
    method: 'get'
  })
}

export const GetProjectUpdate = (id, data) => {
  return request({
    url: `/adminapi/project/update/${id}`,
    method: "put",
    data
  })
}

export const GetProjectModuleUpdate = (id, data) => {
  return request({
    url: `/adminapi/project/module/update/${id}`,
    method: "put",
    data
  })
}


