// <!-- 组件名: status -->
// <!-- 创建日期: 2023/12/28 -->
// <!-- 编写者: Adil -->
import request from "@/utils/request";

/**
 * 新建接口
 * */
export const GetStatusAdd = (data) => {
    return request({
        url: '/adminapi/project/status/add',
        method: 'post',
        data
    })
}

export const GetStatusList = (params) => {
    return request({
        url: '/adminapi/project/status/getList',
        method: 'get',
        params
    })
}


export const GetStatusUpdate = (id, data) => {
    return request({
        url: `/adminapi/project/status/update/${id}`,
        method: "put",
        data
    })
}

export const GetStatusDelete = (id) => {
    return request({
        url: `/adminapi/project/status/delete/${id}`,
        method: 'delete'
    })
}

export const GetStatusListId = (id) => {
    return request({
        url: `/adminapi/project/status/List/${id}`,
        method: 'get'
    })
}
