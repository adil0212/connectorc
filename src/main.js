import Vue from 'vue'

import Cookies from 'js-cookie'

import Element from 'element-ui'
import './assets/styles/element-variables.scss'
import '@/assets/styles/index.scss'
import "@/assets/styles/ruoyi.scss"
import '@/assets/font/iconfont.css'

import VueHighlightJS from 'vue-highlightjs'
import 'highlight.js/styles/atom-one-dark.css'

import App from './App.vue'
import router from './router'
import directive from './directive'
import store from './store'
import plugins from './plugins'

import './assets/icons'
import './permission'

import VueParticles from 'vue-particles'

import VueMeta from 'vue-meta'

// 富文本组件
import Editor from "@/components/Editor"

Vue.component('Editor', Editor)

Vue.use(VueHighlightJS)

Vue.use(directive)

Vue.use(plugins)

Vue.use(VueMeta)

Vue.use(Element, {
  size: Cookies.get('size') || 'medium'
})

Vue.use(VueParticles)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
