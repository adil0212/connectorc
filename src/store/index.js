import Vue from 'vue'
import Vuex from 'vuex'
import settings from "@/store/modules/settings";
import user from "@/store/modules/user";
import app from "@/store/modules/app";
import permission from "@/store/modules/permission";
import getters from "@/store/getters";
import tagsView from "@/store/modules/tagsView";

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    app,
    user,
    tagsView,
    settings,
    permission
  },
  getters
})
