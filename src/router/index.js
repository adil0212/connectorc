import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Layout from "@/layout";

export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login.vue'),
    hidden: true
  },
  {
    path: '/register',
    component: () => import('@/views/register'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error/401'),
    hidden: true
  },
  {
   path: '/preview/:id',
   component: () => import('@/views/project/preview/index.vue'),
   hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/index'),
        name: 'Index',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      }
    ]
  },
  {
    path: '/user',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: 'profile',
        component: () => import('@/views/system/user/profile/index'),
        name: 'Profile',
        meta: { title: '个人中心', icon: 'user' }
      }
    ]
  },
  {
    path: '/project',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: 'particulars/:id',
        component: () => import('@/views/project/particulars/index.vue'),
        name: 'Particulars',
        meta: { title: '项目详情', icon: 'user' }
      },
      {
        path: 'module/:id',
        component: () => import('@/views/project/particulars/Module/index.vue'),
        name: 'Module',
        meta: { title: '模块', icon: 'module' }
      },
      {
        path: 'status/:id',
        component: () => import('@/views/project/particulars/Status/index.vue'),
        name: 'Status',
        meta: { title: '状态码', icon: 'status' }
      },
      {
        path: 'connector/:id',
        component: () => import('@/views/project/particulars/connector/index.vue'),
        name: 'Connector',
        meta: { title: '接口', icon: 'connector' }
      }
    ]
  }
]

export const dynamicRoutes = []

// 防止连续点击多次路由报错
let routerPush = Router.prototype.push;
let routerReplace = Router.prototype.replace;
// push
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(err => err)
}
// replace
Router.prototype.replace = function push(location) {
  return routerReplace.call(this, location).catch(err => err)
}

export default new Router({
  mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})
