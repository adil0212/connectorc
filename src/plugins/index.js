// <!-- 组件名: index -->
// <!-- 创建日期: 2023/12/23 -->
// <!-- 编写者: Adil -->
import cache from "@/plugins/cache";
import auth from "@/plugins/auth";
import tab from "@/plugins/tab";
import modal from "@/plugins/modal";

export default {
  install(Vue) {
    // 页签操作
    Vue.prototype.$tab = tab
    // 认证对象
    Vue.prototype.$auth = auth
    // 缓存对象
    Vue.prototype.$cache = cache
    // 模态框对象
    Vue.prototype.$modal = modal
  }
}
