// <!-- 组件名: index -->
// <!-- 创建日期: 2023/12/26 -->
// <!-- 编写者: Adil -->
import hasRole from './permission/hasRole'
import hasPermi from './permission/hasPermi'

const install = function(Vue) {
  Vue.directive('hasRole', hasRole)
  Vue.directive('hasPermi', hasPermi)
}

if (window.Vue) {
  window['hasRole'] = hasRole
  window['hasPermi'] = hasPermi
  Vue.use(install); // eslint-disable-line
}

export default install
