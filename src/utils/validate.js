// <!-- 组件名: validate -->
// <!-- 创建日期: 2023/12/23 -->
// <!-- 编写者: Adil -->
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}
