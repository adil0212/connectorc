// <!-- 组件名: auth -->
// <!-- 创建日期: 2023/12/23 -->
// <!-- 编写者: Adil -->
import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'

const UserId = 'User-Id'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function getUserId() {
  return Cookies.get(UserId)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function setUserId(id) {
  return Cookies.set(UserId, id)
}
export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function removeuserId() {
  return Cookies.remove(UserId)
}
