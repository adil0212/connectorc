// <!-- 组件名: upload -->
// <!-- 创建日期: 2023/12/25 -->
// <!-- 编写者: Adil -->
import axios from "axios";
import {getToken} from "@/utils/auth";


axios.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getToken()}`
  return config
}, (error) => {
  return Promise.reject(error)
})
function upload(rul, userForm) {
  const params = new FormData()
  for (let i in userForm) {
    params.append(i, userForm[i])
  }
  return axios.post(rul,params, {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  }).then(res => res)
}

export default upload
